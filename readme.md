# Blockbomber

> **Note:** this is a multiplayer-only game. You can run it on a local server or play on an online server
# Overview

![Banner](menu/banner.png)

**Blockbomber** is a Minetest arcade-like game where the goal is to be the last player standing. Collect power-ups to increase your abilities, avoid the bad ones, place bombs strategically and avoid falling (falling is based on center, not on hitbox, so watch out!). You can move through other players, but not through bombs.

Make your own arenas using [**Blockbomber Editor**](https://content.minetest.net/packages/MisterE/blockbomber_editor/)!  Arenas are distributed as mods. If you have made arenas, [**contact maintainer**](https://gitlab.com/mistere123.coding) to add them on the server and possibly in the game.

### Links
- [**Website**](https://www.blockbomber.org/)
- [**Discord**](https://discord.gg/fqcEMNenhA)
- [**Source (GitLab)**](https://gitlab.com/blocknerd-games/blockbomber)

### Credits
- [**threehymns**](https://gitlab.com/threehymns1) — Code/Textures
- [**MisterE**](https://gitlab.com/mistere123.coding) — Code/Textures
- [**temhotaokeaha**](https://gitlab.com/temhotaokeaha) — Banner/Readme improvements
- [**PolySaken**](https://gitlab.com/PolySaken-I-Am) — Code/Textures
# **Controls**
- Movement: [arrow keys]
- Drop bomb: [sneak] (configurable with `/place jump` or `/place sneak`)
- Throw a bomb that you are standing in: [jump] (if player has Spring)
- Punch bomb: [LMB] (with Fist)

# Content

## Powerups

### GOOD

*Stackable*:
- **Bomb**: The more bombs player has, the more they can place at once
- **TNT**: The more TNTs player has, the larger the blast
- **Shoes**: You start out a bit slow. Get shoes to get faster, but take care you don't get too fast and fall off!

*Permanent*: 
- **Spring**: Allows throwing bombs player just placed or is standing in with jump key
- **Fist**: Allows punching bombs the player is next to (but not *in*)

*Temporary*:
- **Compass**: Makes explosions go diagonally as well as orthogonally
- **Heart**: Grants one extra life against bomb blasts, one at a time!

*Other*:
- **Phoenix**: Resurrects dead players

### BAD 
(*bad power-ups are temporary and can be removed by getting another power-up or waiting*)
- **Confused Frog**: Inverts player's movements
- **Snail**: Makes player super slow
- **Coffee**: Makes player *too* fast
- **X-Bomb**: Player can't place bombs til its gone!
- **Party**: Bomb fest!

## Floor blocks
- **Floor**: Regular block
- **Ice**: Slippery block
- **Arrow**: Pushes bombs in its direction
- **Mortar**: Throws bombs to a random location on the board
- **Air**: Players can fall thru holes if they're not careful!

## Wall blocks
- **Crate**: Always drops an item when blasted
- **Brick**: Has a chance to drop an item when blasted
- **Wall**: Can't be broken
