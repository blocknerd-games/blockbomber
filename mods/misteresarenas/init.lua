
local modpath = minetest.get_modpath("misteresarenas")
local schemdir = modpath.."/schems/"




bb_schems.register_arena({
    
    name = "Runabout", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."runabout.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 2, --maximum players
    spawns = {
        vector.new(9,1,11), -- the spawn locations, needs as many as max players
        vector.new(9,1,5),
    }, --a table of spawn locations
    spectator_pos = vector.new(9,7,8), -- spectator position, should be above and in the center of the arena.
})




bb_schems.register_arena({
    
    name = "Three Ships", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."threeships.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 6, --maximum players
    spawns = {
        vector.new(2,1,8), -- the spawn locations, needs as many as max players
        vector.new(22,1,16),
        vector.new(16,1,22),
        vector.new(8,1,2),
        vector.new(15,1,9),
        vector.new(9,1,15),
    }, --a table of spawn locations
    spectator_pos = vector.new(12,17,12), -- spectator position, should be above and in the center of the arena.
})



bb_schems.register_arena({
    
    name = "Loopy", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."loopy.mts", --the absolute file location of the schematic
    min = 4, --minimum players
    max = 12, --maximum players
    spawns = {
        vector.new(8,1,8), -- the spawn locations, needs as many as max players
        vector.new(25,1,25),
        vector.new(8,1,25),
        vector.new(25,1,8),
        vector.new(20,1,6),
        vector.new(20,1,27),
        vector.new(13,1,27), 
        vector.new(13,1,6),
        vector.new(6,1,13),
        vector.new(27,1,13),
        vector.new(27,1,20),
        vector.new(6,1,20),
    }, --a table of spawn locations
    spectator_pos = vector.new(17,21,17), -- spectator position, should be above and in the center of the arena.
})



bb_schems.register_arena({
    
    name = "The Inner Hold", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."innerhold.mts", --the absolute file location of the schematic
    min = 6, --minimum players
    max = 12, --maximum players
    spawns = {
        vector.new(26,1,26),
        vector.new(8,1,8),
        vector.new(26,1,8),
        vector.new(8,1,26),
        vector.new(7,1,20),
        vector.new(27,1,20),
        vector.new(27,1,14),
        vector.new(7,1,14),
        vector.new(14,1,7),
        vector.new(14,1,27),
        vector.new(20,1,27),
        vector.new(20,1,7),
    }, --a table of spawn locations
    spectator_pos = vector.new(17,20,17), -- spectator position, should be above and in the center of the arena.
})


bb_schems.register_arena({
    
    name = "Dance Floor", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."dancefloor.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(20,1,20),
        vector.new(9,1,9),
        vector.new(20,1,9),
        vector.new(9,1,20),
     }, --a table of spawn locations
    spectator_pos = vector.new(14,11,14), -- spectator position, should be above and in the center of the arena.
})


bb_schems.register_arena({
    
    name = "Bow", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."bow.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 2, --maximum players
    spawns = {
        vector.new(8,1,20),
        vector.new(7,1,7),
     }, --a table of spawn locations
    spectator_pos = vector.new(8,9,13), -- spectator position, should be above and in the center of the arena.
})


bb_schems.register_arena({
    
    name = "Pinch Point", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."pinchpoint.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(31,1,21),
        vector.new(5,1,4),
        vector.new(31,1,4),
        vector.new(5,1,21),
       
     }, --a table of spawn locations
    spectator_pos = vector.new(18,16,13), -- spectator position, should be above and in the center of the arena.
})



bb_schems.register_arena({
    
    name = "Tiny Classic", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."classic_tiny.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(11,1,11),
        vector.new(3,1,3),
        vector.new(11,1,3),
        vector.new(3,1,11),
       
     }, --a table of spawn locations
    spectator_pos = vector.new(7,8,7), -- spectator position, should be above and in the center of the arena.
})


bb_schems.register_arena({
    
    name = "The Cold Road", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."thecoldroad.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(16,1,12),
        vector.new(5,1,5),
        vector.new(15,1,5),
        vector.new(15,1,12),       
     }, --a table of spawn locations
    spectator_pos = vector.new(10,11,10), -- spectator position, should be above and in the center of the arena.
})



bb_schems.register_arena({
    
    name = "Notch", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."notch.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(21,1,16),
        vector.new(8,1,8),
        vector.new(8,1,16),
        vector.new(21,1,8),       
     }, --a table of spawn locations
    spectator_pos = vector.new(15,14,12), -- spectator position, should be above and in the center of the arena.
})



bb_schems.register_arena({
    
    name = "GristMills", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."gristmills.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(12,1,16),
        vector.new(3,1,3),
        vector.new(3,1,16),
        vector.new(12,1,3),       
     }, --a table of spawn locations
    spectator_pos = vector.new(7,10,9), -- spectator position, should be above and in the center of the arena.
})



-- bb_schems.register_arena({
    
--     name = "Drone", --the name of the arenas
--     author = "cheeseball", --the author of the arenas
--     schem = schemdir.."drone.mts", --the absolute file location of the schematic
--     min = 2, --minimum players
--     max = 4, --maximum players
--     spawns = {
--         vector.new(0,1,0),
--         vector.new(22,1,35),
--         vector.new(0,1,35),
--         vector.new(22,1,0),       
--      }, --a table of spawn locations
--     spectator_pos = vector.new(14,22,18), -- spectator position, should be above and in the center of the arena.
-- })



-- bb_schems.register_arena({
    
--     name = "Backyard", --the name of the arenas
--     author = "MisterE", --the author of the arenas
--     schem = schemdir.."backyard.mts", --the absolute file location of the schematic
--     min = 2, --minimum players
--     max = 4, --maximum players
--     spawns = {
--         vector.new(9,1,2),
--         vector.new(8,2,28),
--         vector.new(5,1,14),
--         vector.new(12,2,16),       
--      }, --a table of spawn locations
--     spectator_pos = vector.new(9,12,16), -- spectator position, should be above and in the center of the arena.
-- })


-- 3d arenas are... not great

-- bb_schems.register_arena({
    
--     name = "Hard Stair", --the name of the arenas
--     author = "MisterE", --the author of the arenas
--     schem = schemdir.."hardstair.mts", --the absolute file location of the schematic
--     min = 2, --minimum players
--     max = 4, --maximum players
--     spawns = {
--         vector.new(9,1,5),
--         vector.new(7,3,9),
--         vector.new(10,5,9),
--         vector.new(10,7,6),
--      }, --a table of spawn locations
--     spectator_pos = vector.new(9,35,7), -- spectator position, should be above and in the center of the arena.
-- })