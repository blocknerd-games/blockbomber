-- bb_schems is an api table 
-- bb_schems.arenas is a table of arena definitions
-- an arena definition requires the following fields:

--     name = string, --the name of the arenas
--     author = string, --the author of the arenas
--     schem = string, --the absolute file location of the schematic
--     min = number, --minimum players
--     max = number, --maximum players
--     spawns = table, --a table of spawn locations
--         --it has entries that follow the format:
--         {x=number,y=number,z=number},
--     spectator_pos = vector location




bb_schems = {}
bb_schems.arenas = {}
-- arena registration

local modpath = minetest.get_modpath("bb_schems")
local schemdir = modpath.."/schems/"

bb_schems.register_arena = function( n_table )
    table.insert( bb_schems.arenas , n_table )
end

-- gets a random arena from available arenas for the number of players available.
-- input: number, player count
-- output: table, the arena which is chosen

bb_schems.get_arena_by_playercount = function( count )
    -- make a list of available arenas
    local available = {}
    -- add arenas based on players
    for _ , arena in pairs( bb_schems.arenas ) do
        if count >= arena.min and count <= arena.max then
            table.insert( available , arena )
        end
    end
    -- choose one, prevent the same arena from running twice, if possible

    local chosen = math.random( 1 , #available )
    if #available > 1 and bb_schems.last_arena and bb_schems.last_arena == available[ chosen ].name then
        while bb_schems.last_arena == available[ chosen ].name do
            chosen = math.random( 1 , #available )
        end
    end

    --return the chosen arena
    bb_schems.last_arena = available[ chosen ].name
    return available[ chosen ]
end





-- searches the arenas and finds the one by the name given
-- input: arena name
-- output: arena table
bb_schems.get_arena_by_name = function(name)
    local ret
    for _, arena in pairs( bb_schems.arenas) do
        if arena.name == name then
            ret = arena
        end
    end
    return ret
end

-- places the arena at the origin.
-- input: arena_name
-- places the arena at 0,0

bb_schems.place_arena = function( name ) 

    local arena = bb_schems.get_arena_by_name( name )

    -- clear the board by placing an empty air schem
    minetest.place_schematic(vector.new(0, 0, 0), schemdir.."clear.mts")
    -- place the new schem
    minetest.place_schematic(vector.new(0, 0, 0), arena.schem)

    
    minetest.chat_send_all("Loading Arena ["..name.."], made by ["..arena.author.."]")

end










bb_schems.register_arena({
    
    name = "Four Leaf Clover", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."4_leaf_clover.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(2,1,2),
        vector.new(19,1,19),
        vector.new(2,1,19),
        vector.new(19,1,2),
    }, --a table of spawn locations
    spectator_pos = vector.new(10,14,10), -- spectator position
})


bb_schems.register_arena({
    
    name = "Aech", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."Aech.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(4,1,2),
        vector.new(15,1,18),
        vector.new(15,1,2),
        vector.new(4,1,18),
    }, --a table of spawn locations
    spectator_pos = vector.new(10,16,10), -- spectator position
})


bb_schems.register_arena({
    
    name = "iheartBB", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."iheartbb.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 3, --maximum players
    spawns = {
        vector.new(2,1,4),
        vector.new(12,1,4),
        vector.new(7,1,12),
    }, --a table of spawn locations
    spectator_pos = vector.new(7,12,7), -- spectator position
})


bb_schems.register_arena({
    
    name = "BombasticSix", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."bombasticsix.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 6, --maximum players
    spawns = {
        vector.new(1,1,1),
        vector.new(19,1,10),
        vector.new(1,1,10),
        vector.new(19,1,1),
        vector.new(10,1,1),
        vector.new(10,1,10)
    }, --a table of spawn locations
    spectator_pos = vector.new(10,11,6), -- spectator position
})


bb_schems.register_arena({
    
    name = "Classic", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."Classic.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(1,1,1),
        vector.new(17,1,17),
        vector.new(1,1,17),
        vector.new(17,1,1),        
    }, --a table of spawn locations
    spectator_pos = vector.new(9,16,9), -- spectator position
})



bb_schems.register_arena({
    
    name = "FourWays", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."fourways.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(5,1,2),
        vector.new(13,1,12),
        vector.new(13,1,2),
        vector.new(5,1,12),        
    }, --a table of spawn locations
    spectator_pos = vector.new(9,13,7), -- spectator position
})



bb_schems.register_arena({
    
    name = "NineIsles", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."nineisles.mts", --the absolute file location of the schematic
    min = 3, --minimum players
    max = 8, --maximum players
    spawns = {
        vector.new(2,1,2),
        vector.new(14,1,14),
        vector.new(2,1,14),
        vector.new(14,1,2),  
        vector.new(16,1,8),
        vector.new(0,1,8),
        vector.new(8,1,16),
        vector.new(8,1,0),   
    }, --a table of spawn locations
    spectator_pos = vector.new(8,13,8), -- spectator position
})


bb_schems.register_arena({
    
    name = "Asterisk", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."asterisk.mts", --the absolute file location of the schematic
    min = 3, --minimum players
    max = 8, --maximum players
    spawns = {
        vector.new(2,1,2),
        vector.new(17,1,17),
        vector.new(17,1,2),
        vector.new(2,1,17),  
        vector.new(1,1,10),
        vector.new(18,1,9),
        vector.new(9,1,1),
        vector.new(10,1,18),  
    }, --a table of spawn locations
    spectator_pos = vector.new(10,15,10), -- spectator position
})




bb_schems.register_arena({
    
    name = "Diamond", --the name of the arenas
    author = "MisterE", --the author of the arenas
    schem = schemdir.."diamond.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(1,1,8),
        vector.new(15,1,8),
        vector.new(8,1,1),
        vector.new(8,1,15),        
    }, --a table of spawn locations
    spectator_pos = vector.new(8,12,8), -- spectator position
})



bb_schems.register_arena({
    
    name = "ToThePoint", --the author of the arenas
    author = "MisterE",
    schem = schemdir.."tothepoint.mts", --the absolute file location of the schematic
    min = 2, --minimum players
    max = 2, --maximum players
    spawns = {
        vector.new(12,1,5),
        vector.new(1,1,1),  
    }, --a table of spawn locations
    spectator_pos = vector.new(7,13,4), -- spectator position
})










































-- bb_schems.spawns = {}

-- -- tools to make level creation easier

-- -- a spawn indicator node
-- minetest.register_node("bb_nodes:spawn", {
--     description = "spawn_location",
--     tiles = {"bb_schems_spawn.png"},
--     groups = {not_in_creative_inventory = 1},
-- }) 

-- minetest.register_privilege( "level" , "allows level creation" )


-- minetest.register_chatcommand( "setspawn" , {
--     privs = {
--         level = true,
--     },
--     func = function( name , param )
--         local player = minetest.get_player_by_name( name ) 
--         if not player then return end
--         if not minetest.get_modpath("worldedit") then
--             return "you need worldedit for this, please install it."
--         end
--         local pos = player:get_pos()
--         pos = vector.round( pos )
--         table.insert( bb_schems.spawns , pos )
--         minetest.set_node( pos , { name = "bb_nodes:spawn" } )
--     end,
-- })

-- minetest.register_chatcommand( "clearspawns" , {
--     privs = {
--         level = true,
--     },
--     func = function( name , param )
--         local player = minetest.get_player_by_name( name ) 
--         if not player then return end
--         for _ , pos in pairs( bb_schems.spawns ) do
--             minetest.set_node( pos , { name = "air" } )
--         end
--         bb_schems.spawns = {}
--     end,
-- })

-- minetest.register_chatcommand( "savearena" , {
--     privs = {
--         level = true,
--     },
--     func = function( name , param )
--         local player = minetest.get_player_by_name( name ) 
--         if not player then return end
--         if not minetest.get_modpath("worldedit") then
--             return "you need worldedit for this, please install it"
--         end
--         local pos1 = worldedit.pos1[name]
--         local pos2 = worldedit.pos2[name]
--         local path = minetest.get_worldpath().."/arenas"
--         for _ , pos in pairs( bb_schems.spawns ) do
--             minetest.set_node( pos , { name = "air" } )
--         end
--         if not path then minetest.mkdir(path) end
--         local file = path .."/".. param .. ".mts"
--         minetest.create_schematic(pos1, pos2, worldedit.probability_list[name], file)


        
--     end,
-- })