bb_loop = {}

local storage = minetest.get_mod_storage()
bb_loop.storage = storage

bb_loop.leaderboard = minetest.deserialize(storage:get_string("leaderboard")) or {}

-- settings


bb_loop.debug = false
bb_loop.loading_time = 7 -- time for loading phase
bb_loop.celebrate_time = 5 --- time for celebration phase
bb_loop.suddendeath_timer = 180 -- time for regular gameplay until the game gives a little hint to hurry up

-- Global Vars declared for reference and init
bb_loop.winner = ""
bb_loop.gamestate = "waiting"
bb_loop.suddendeath = false
bb_loop.firstload = true
bb_loop.waitingtimer = 0 
bb_loop.loadingtimer = 0
bb_loop.runningtimer = 0
bb_loop.celebratetimer = 0
bb_loop.current_arena = nil
bb_loop.loopcounter = 1 -- a counter that is unique for each iteration of the game



bb_loop.registered_on_loads = {}
bb_loop.registered_on_starts = {}
bb_loop.registered_playsteps = {}
bb_loop.registered_on_celebrates = {}
bb_loop.registered_on_suddendeaths = {}


local path = minetest.get_modpath("bb_loop")
dofile(path.."/api.lua")



bb_loop.register_on_load(function()    
    for _ , player in ipairs(minetest.get_connected_players()) do
        local p_name = player:get_player_name()
        minetest.close_formspec(p_name, "")
    end
    bb_loop.Start()
end)


bb_loop.register_on_start(function()
    for _,player in pairs(minetest.get_connected_players()) do
        local p_name = player:get_player_name()
        player:set_inventory_formspec(bb_loop.get_inv_formspec(p_name))
    end
    bb_loop.first_sd = true --first_sd means first_sd detection
end)


bb_loop.register_playstep(function()

    -- check for winners
    local winner = bb_loop.check_for_winner()
    if winner then 
        if #minetest.get_connected_players() > 1 then
            bb_loop.winner = winner
        end
        return true 
    end

    if bb_loop.runningtimer > bb_loop.suddendeath_timer then
        bb_loop.suddendeath = true
        --set in last phase, only do this once, first_sd means first_sd detection
        if bb_loop.first_sd then
            bb_loop.first_sd = false

            for _,func in ipairs(bb_loop.registered_on_suddendeaths) do
                func()
            end

            for _,player in pairs(minetest.get_connected_players()) do
                minetest.sound_play({
                    name = "alarm",
                    gain = 0.1,
                }, {
                    to_player = player:get_player_name(),        
                    fade = 0.0,   -- default, change to a value > 0 to fade the sound in
                    pitch = 1.0,  -- default
                }, true)
            end
        end

    end
end)




bb_loop.register_on_celebrate(function(winner)

    -- move player to spectator if only one player left
    if #minetest.get_connected_players() == 1 then
        if bb_loop.current_arena and bb_loop.current_arena.spectator_pos then
            for _,player in pairs(minetest.get_connected_players()) do 
                player:set_pos(bb_loop.current_arena.spectator_pos)
            end
        end    
    end
    
    --keep track of who wins in the leaderboard table
    if bb_loop.winner ~= nil and bb_loop.winner ~= "" then
        if bb_loop.leaderboard[bb_loop.winner] then
            bb_loop.leaderboard[bb_loop.winner] = bb_loop.leaderboard[bb_loop.winner] + 1
        else
            bb_loop.leaderboard[bb_loop.winner] = 1
        end
        storage:set_string("leaderboard", minetest.serialize(bb_loop.leaderboard))
    end

    
    -- increment the loop counter
    bb_loop.loopcounter = bb_loop.loopcounter + 1

    -- clear board
    for _,obj in pairs(minetest.get_objects_inside_radius(vector.new(0,0,0), 200)) do

        local ent = obj:get_luaentity()
        if ent and ent.name == "__builtin:item" then
            obj:remove()
        end
    end


    minetest.after(0.1,function()
        bb_loop.runningtimer = 0
        bb_loop.suddendeath = false
    end)
end)










-- move to bb_powerup
-- item jump globalstep
local jumptimer = 0

minetest.register_globalstep(function(dtime)
    if not(bb_loop.gamestate == "running" ) then return end
    jumptimer = jumptimer + dtime 
    if jumptimer > 1 then 
        jumptimer = 0

        for id, ent in pairs(minetest.luaentities) do 
            if ent.name == "__builtin:item" then
                ent.object:add_velocity(vector.new(0,2,0))

            end
        end
    end

end)


