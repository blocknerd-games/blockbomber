bb_nodes = {}

bb_nodes.slope_box = {
	type = "fixed",
	fixed = {
		{-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
		{-0.5000, -0.5000, 0.3750, 0.5000, 0.3750, 0.2500},
		{-0.5000, -0.5000, 0.2500, 0.5000, 0.2500, 0.1250},
		{-0.5000, -0.5000, 0.1250, 0.5000, 0.1250, 0.000},
		{-0.5000, -0.5000, -0.000, 0.5000, 0.000, -0.1250},
		{-0.5000, -0.5000, -0.1250, 0.5000, -0.1250, -0.2500},
		{-0.5000, -0.5000, -0.2500, 0.5000, -0.2500, -0.3750},
		{-0.5000, -0.5000, -0.3750, 0.5000, -0.3750, -0.5000}
	}
}
bb_nodes.inner_slope_box = {
	type = "fixed",
	fixed = {
		{-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
		{-0.5000, -0.5000, 0.3750, 0.5000, 0.3750, 0.2500},
		{-0.5000, -0.5000, 0.2500, 0.5000, 0.2500, 0.1250},
		{-0.5000, -0.5000, 0.1250, 0.5000, 0.1250, 0.000},
		{-0.5000, -0.5000, -0.000, 0.5000, 0.000, -0.1250},
		{-0.5000, -0.5000, -0.1250, 0.5000, -0.1250, -0.2500},
		{-0.5000, -0.5000, -0.2500, 0.5000, -0.2500, -0.3750},
		{-0.5000, -0.5000, -0.3750, 0.5000, -0.3750, -0.5000},

        {-0.5000, -0.5000, -0.5000,   -0.3750  , 0.5000 ,0.5000},
		{-0.5000, -0.5000, -0.3750,   -0.2500  , 0.3750 ,0.5000},
		{-0.5000, -0.5000, -0.2500,   -0.1250  , 0.2500 ,0.5000},
		{-0.5000, -0.5000, -0.1250,   -0.000   , 0.1250 ,0.5000},
		{-0.5000, -0.5000, 0.000,   0.1250 , 0.000  ,0.5000},
		{-0.5000, -0.5000, 0.1250,  0.2500 , -0.1250, 0.5000},
		{-0.5000, -0.5000, 0.2500,  0.3750 , -0.2500,0.5000},
		{-0.5000, -0.5000, 0.3750,  0.5000 , -0.3750,0.5000}
	}
}


bb_nodes.outer_slope_box = {
	type = "fixed",
	fixed = {
		{0.5000, -0.5000, 0.5000, 0.3750, 0.5000, 0.3750},
		{0.3750, -0.5000, 0.5000, 0.2500, 0.3750, 0.3750},
		{0.5000, -0.5000, 0.3750, 0.3750, 0.3750, 0.2500},
		{0.2500, -0.5000, 0.5000, 0.1250, 0.2500, 0.3750},
		{0.3750, -0.5000, 0.3750, 0.2500, 0.2500, 0.2500},
		{0.5000, -0.5000, 0.2500, 0.3750, 0.2500, 0.1250},
		{0.1250, -0.5000, 0.5000, 0.000, 0.1250, 0.3750},
		{0.2500, -0.5000, 0.3750, 0.1250, 0.1250, 0.2500},
		{0.3750, -0.5000, 0.2500, 0.2500, 0.1250, 0.1250},
		{0.5000, -0.5000, 0.1250, 0.3750, 0.1250, 0.000},
		{-0.000, -0.5000, 0.5000, -0.1250, 0.000, 0.3750},
		{0.1250, -0.5000, 0.3750, 0.000, 0.000, 0.2500},
		{0.2500, -0.5000, 0.2500, 0.1250, 0.000, 0.1250},
		{0.3750, -0.5000, 0.1250, 0.2500, 0.000, 0.000},
		{0.5000, -0.5000, 0.000, 0.3750, 0.000, -0.1250},
		{0.1250, -0.5000, 0.5000, -0.2500, -0.1250, 0.3750},
		{0.000, -0.5000, 0.3750, -0.1250, -0.1250, 0.2500},
		{0.1250, -0.5000, 0.2500, 0.000, -0.1250, 0.1250},
		{0.2500, -0.5000, 0.1250, 0.1250, -0.1250, 0.000},
		{0.3750, -0.5000, 0.000, 0.2500, -0.1250, -0.1250},
		{0.5000, -0.5000, -0.1250, 0.3750, -0.1250, -0.2500},
		{-0.2500, -0.5000, 0.5000, -0.3750, -0.2500, 0.3750},
		{-0.1250, -0.5000, 0.3750, -0.2500, -0.2500, 0.2500},
		{-0.000, -0.5000, 0.2500, -0.1250, -0.2500, 0.1250},
		{0.1250, -0.5000, 0.1250, 0.000, -0.2500, 0.000},
		{0.2500, -0.5000, 0.000, 0.1250, -0.2500, -0.1250},
		{0.3750, -0.5000, -0.1250, 0.2500, -0.2500, -0.2500},
		{0.5000, -0.5000, -0.2500, 0.3750, -0.2500, -0.3750},
		{-0.3750, -0.5000, 0.5000, -0.5000, -0.3750, 0.3750},
		{-0.2500, -0.5000, 0.3750, -0.3750, -0.3750, 0.2500},
		{-0.1250, -0.5000, 0.2500, -0.2500, -0.3750, 0.1250},
		{-0.000, -0.5000, 0.1250, -0.1250, -0.3750, 0.000},
		{0.1250, -0.5000, 0.000, 0.000, -0.3750, -0.1250},
		{0.2500, -0.5000, -0.1250, 0.1250, -0.3750, -0.2500},
		{0.3750, -0.5000, -0.2500, 0.2500, -0.3750, -0.3750},
		{0.5000, -0.5000, -0.3750, 0.3750, -0.3750, -0.5000}
	}
}


minetest.register_tool("bb_nodes:breaker", {
    description = "Node Breaker",
    inventory_image = "bb_nodes_breaker.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            cracky = {
                maxlevel = 3,
                uses = 2000,
                times = { [1]=0.20, [2]=0.20, [3]=0.20 }
            },
        },
    },
})


minetest.register_alias('mapgen_stone', 'air')
minetest.register_alias('mapgen_water_source', 'air')
minetest.register_alias('mapgen_river_water_source', 'air')


function bb_nodes.register_floor_set(name,desc,texture)
        
    minetest.register_node("bb_nodes:"..name.."_slope", {
        description = desc.." Slope",
        paramtype = "light",
        drawtype = "mesh",
        --drawtype = "nodebox",
        mesh = "slope.obj",
        node_box = bb_nodes.slope_box,
        paramtype2 = "facedir",
        selection_box = bb_nodes.slope_box,
        collision_box = bb_nodes.slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1,solid=1},
    }) 


    minetest.register_node("bb_nodes:"..name, {
        description = desc,
        paramtype = "light",
        tiles = {texture},
        groups = {cracky = 3, floor = 1,solid=1},
    }) 

    minetest.register_node("bb_nodes:"..name.."_slope_inner", {
        description = desc.." Slope Inner",
        paramtype = "light",
        drawtype = "mesh",
        mesh = "slope_inside.obj",
        paramtype2 = "facedir",
        selection_box = bb_nodes.inner_slope_box,
        collision_box = bb_nodes.inner_slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1,solid=1},
    }) 

    minetest.register_node("bb_nodes:"..name.."_slope_outer", {
        description = desc.." Slope Outer",
        paramtype = "light",
        drawtype = "mesh",
        mesh = "slope_outside.obj",
        paramtype2 = "facedir",
        selection_box = bb_nodes.outer_slope_box,
        collision_box = bb_nodes.outer_slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1,solid=1},
    }) 

end


-- classic set


bb_nodes.register_floor_set("floor","Floor","bb_nodes_floor.png")



minetest.register_node("bb_nodes:floor_with_arrow", {
    description = "Floor Arrow\n Pushes bombs in this direction",
    tiles = {"bb_nodes_floor_with_arrow.png","bb_nodes_floor.png"},
    groups = {cracky = 3, floor = 1, arrow = 1,solid=1},
    paramtype = "light",
    paramtype2 = "facedir",
}) 


minetest.register_node("bb_nodes:floor_with_mortar", {
    description = "Floor Mortar\n Collects and randomly throws bombs",
    tiles = {"bb_nodes_floor_with_mortar.png","bb_nodes_floor.png"},
    groups = {cracky = 3, floor = 1, mortar = 1,solid=1},
    paramtype = "light",
    paramtype2 = "facedir",
}) 


minetest.register_node("bb_nodes:brick", {
    description = "Removable Wall",
    tiles = {"bb_nodes_brick.png^[transformFX","bb_nodes_brick.png"},
    paramtype = "light",
    groups = {cracky = 3,penetrable = 1,removable = 1,powerup = 1,solid=1},
}) 


minetest.register_node("bb_nodes:crate", {
    description = "Removable Crate",
    tiles = {"bb_nodes_crate.png"},
    paramtype = "light",
    groups = {cracky = 3,penetrable = 1,removable = 1,powerup = 2,solid=1},
}) 


minetest.register_node("bb_nodes:ice", {
    description = "Slippery Ice",
    tiles = {"bb_nodes_ice.png^[opacity:200"},
    paramtype = "light",
    use_texture_alpha = true,
    drawtype = "glasslike_framed_optional",
    groups = {cracky = 3, slippery = 9, floor = 1,solid=1},
}) 


minetest.register_node("bb_nodes:wall", {
    description = "Solid Wall",
    tiles = {"bb_nodes_wall.png"},
    paramtype = "light",
    groups = {cracky = 3,penetrable = 1,solid=1},
}) 


minetest.register_node("bb_nodes:fence", {
    description = "Fence",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence.obj",
    paramtype2 = "facedir",
    selection_box =  {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750}
        },
    },
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750}
        },
    },
    tiles = {"fence_classic.png"},
    groups = {cracky = 3,penetrable = 1,solid=1},
}) 

minetest.register_node("bb_nodes:fence_corner", {
    description = "Fence (corner)",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence_corner.obj",
    paramtype2 = "facedir",
    selection_box =  {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            { 0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    collision_box ={
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            { 0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    tiles = {"fence_classic.png"},
    
    groups = {cracky = 3,penetrable = 1,solid=1},
}) 




-- forest set


bb_nodes.register_floor_set("forest_floor","Forest Floor","bb_nodes_floor_forest.png")



minetest.register_node("bb_nodes:forest_floor_with_arrow", {
    description = "Floor Arrow\n Pushes bombs in this direction",
    tiles = {"bb_nodes_floor_with_arrow_forest.png","bb_nodes_floor_forest.png"},
    groups = {cracky = 3, floor = 1, arrow = 1,solid=1},
    paramtype = "light",
    paramtype2 = "facedir",
}) 


minetest.register_node("bb_nodes:forest_floor_with_mortar", {
    description = "Floor Mortar\n Collects and randomly throws bombs",
    tiles = {"bb_nodes_floor_with_mortar_forest.png","bb_nodes_floor_forest.png"},
    groups = {cracky = 3, floor = 1, mortar = 1,solid=1},
    paramtype = "light",
    paramtype2 = "facedir",
}) 


minetest.register_node("bb_nodes:bush", {
    description = "Bush (forest brick)",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "bush.obj",
    paramtype2 = "facedir",
    tiles = {"bush.png"},
    groups = {cracky = 3,penetrable = 1,removable = 1,powerup = 1,solid=1},
}) 


minetest.register_node("bb_nodes:acorn", {
    description = "Acorn (forest Crate)",
    tiles = {"acorn_top.png","acorn_bottom.png","acorn_side.png"},
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.4375, -0.3750, -0.4375, 0.4375, 0.3750, 0.4375},
            {-0.3125, -0.5000, -0.3125, 0.3125, -0.3750, 0.3125},
            {-0.3750, 0.3750, -0.3750, 0.3750, 0.4375, 0.3750}
        }
    },
    paramtype = "light",
    groups = {cracky = 3,penetrable = 1,removable = 1,powerup = 2,solid=1},
}) 


minetest.register_node("bb_nodes:mud", {
    description = "Mud (forest Ice)",
    tiles = {"mud_top.png","mud_top.png","mud_side.png"},
    paramtype = "light",
    use_texture_alpha = true,
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, -0.5000, 0.5000, .5-(1/16), 0.5000 },
        },
    },
    groups = {cracky = 3, slippery = 9, floor = 1,solid=1},
}) 


minetest.register_node("bb_nodes:rock", {
    description = "Rock (forest Solid Wall)",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "rock.obj",
    paramtype2 = "facedir",
    tiles = {"rock.png"},
    groups = {cracky = 3,penetrable = 1,solid=1},
}) 


minetest.register_node("bb_nodes:fence_forest", {
    description = "Fence",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence.obj",
    paramtype2 = "facedir",
    selection_box =  {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
        },
    },
    collision_box ={
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
        },
    },
    tiles = {"fence.png"},
    groups = {cracky = 3,penetrable = 1,solid=1},

}) 

minetest.register_node("bb_nodes:fence_forest_corner", {
    description = "Fence (corner)",
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence_corner.obj",
    paramtype2 = "facedir",
    selection_box =  {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            { 0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    collision_box ={
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            { 0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    tiles = {"fence.png"},
    groups = {cracky = 3,penetrable = 1,solid=1},

}) 

