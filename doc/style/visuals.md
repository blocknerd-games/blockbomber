# Block Bomber Visual Style Guide

## Item Visuals

 * Use the Zughy32 palette, provided in `palette.png`
 * Item textures (such as powerups) should have a solid, dark outline (except for very small parts that would be too large otherwise)
 * Use dithering and gradients to give items detail and depth
 * Keep item textures 32x32
 
## Tile Visuals

 * Keep tile textures simple and mostly flat; avoid gradients and dithering, but simple 2-step celshading is fine.
 * Keep tile textures 16x16